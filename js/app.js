var app = angular.module('TestNg', ['ngRoute', 'ngResource', 'ngSanitize', 'ui.bootstrap']);

app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: 'partials/login.html',
        controller: 'loginController'
    }).when('/tickets', {
        templateUrl: 'partials/list.html',
        controller: 'ticketsController'
    }).when('/tickets/details/', {
        templateUrl: 'partials/details.html',
        controller: 'ticketsController'
    }).when('/test', {
        templateUrl: 'test.html'
    }).otherwise({
        redirectTo: '/'
    });
}]);

app.factory('ticketListService', ['$resource', '$sanitize', function ($resource, $sanitize) {
    var factory = {selectedTicket: null};
    factory.getTickets = $resource('data/tickets.json');
    factory.setSelectedTicket = function (ticket) {
        factory.selectedTicket = ticket;
        factory.selectedTicket.details = $sanitize(factory.selectedTicket.details);
    };
    return factory
}]);

app.controller('loginController', ['$scope', function ($scope) {
    $scope.credentails = {
        username: 'sss',
        password: ''
    };
    $scope.onLoginSubmit = function () {
        console.log($scope.credentails.username);
    };
}]);

app.filter('page', function () {
    return function (arr, page, limit) {
        var start = (page-1) * limit;
        var end = start + limit;
        return (arr || []).slice(start, end);
    };
});

app.controller('ticketsController', ['$scope', 'filterFilter', '$location', 'ticketListService',
    function ($scope, filterFilter, $location, ticketListService) {
        $scope.tickets = [];
        $scope.selectedTicket = ticketListService.selectedTicket;
        ticketListService.getTickets.get({}, function (response) {
            $scope.tickets = response.results;
        });

        $scope.showTicketDetail = function (ticket) {
            ticketListService.setSelectedTicket(ticket);
            $location.path('/tickets/details');
        };

        if ($location.path() == '/tickets/details/' && !$scope.selectedTicket) {
            $location.path('/tickets');
        }

        $scope.search = {title: ''};
        $scope.maxItemsPerPage = 15;
        $scope.currentPage = 1;
        $scope.pageChanged = function () {
        };
        $scope.onSearchKeyChanged = function () {
            $scope.currentPage = 1;
        };

        $scope.orderCol = '';
        $scope.changeOrder = function (col) {
            if ($scope.orderCol == col) {   //Toggle sort order
                if (col[0] == '-') {
                    col = col.slice(col, s.length)
                } else {
                    col = '-' + col;
                }
            }
            $scope.orderCol = col;
        };
    }]);
