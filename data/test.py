import json
s = open('tickets.json','r').read();
data = json.loads(s)


statuses = {}
priorities = {}
tickets = data['results']
for ticket in tickets:
	priorities[ticket['priority']['id']]=ticket['priority']
	statuses[ticket['status']['id']]=ticket['status']

status=[]
for i in statuses:
    status.append(statuses[i])
status  = json.dumps(status, indent=4, sort_keys=True)
f=open('status.json','w');
f.write(status)
f.flush()
f.close()

priority=[]
for i in priorities:
    priority.append(priorities[i])


priority = json.dumps(priority, indent=4, sort_keys=True)
f=open('priority.json','w');
f.write(priority)
f.flush()
f.close()
